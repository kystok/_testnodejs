"use strict";
let db = require('../config.json').db;
let mongoose = require('mongoose'),
    crypto = require('crypto'),
    DB = mongoose.connect(db.url),
    User = require('./models/user.js');

module.exports = {
    createUser,
    login,
    getUsers,
    sendDibInFriends,
    okAddFriend,
    infoDib,
    getFriends,
    getUserInfo,
};

async function getFriends(login=null) {
    if (login) {
        let user = await User.findOne({login});
        let k = 0, tmp =[], usertmp;
        while (k !== user.friends.length) {
            usertmp = await User.findOne({_id: user.friends[k]});
            delete usertmp._doc.password;
            delete usertmp._doc.inDibInFriends;
            delete usertmp._doc.outDibInFriends;
            delete usertmp._doc.friends;
            tmp.push(usertmp._doc);
            k++;
        }
        let friends = [...tmp];
        return {status: 'ok', data: friends}
    }else {
        return {status: 'error'}
    }
}

async function infoDib(login = null) {
    if (login) {
        let tmp = [];
        let result = {inDibInFriends: {}, outDibInFriends: {}};
        let usertmp;
        let user = await User.findOne({login});
        let k = 0;
        while (k !== user.inDibInFriends.length) {
            usertmp = await User.findOne({_id: user.inDibInFriends[k]});
            delete usertmp._doc.password;
            delete usertmp._doc.inDibInFriends;
            delete usertmp._doc.outDibInFriends;
            delete usertmp._doc.friends;
            tmp.push(usertmp._doc);
            k++;
        }
        result.inDibInFriends = [...tmp];
        k = 0;
        while (k !== user.outDibInFriends.length) {
            usertmp = await User.findOne({_id: user.outDibInFriends[k]});
            delete usertmp._doc.password;
            delete usertmp._doc.inDibInFriends;
            delete usertmp._doc.outDibInFriends;
            delete usertmp._doc.friends;
            tmp.push(usertmp._doc);
            k++;
        }
        result.outDibInFriends =  [...tmp];
        return {status: 'ok', data: result}
    } else {
        return {status: 'error'}
    }
}

async function okAddFriend({friendLogin = null, myLogin = null}) {
    if (friendLogin && myLogin) {
        let my = await User.findOne({login: myLogin});
        let friend = await User.findOne({login: friendLogin});
        let i = my.outDibInFriends.indexOf(String(friend._doc._id));
        if (i === -1) return {status: 'error', error: 'Заявка не была отправлена'};
        my.outDibInFriends.splice(i, 1);
        i = friend.inDibInFriends.indexOf(String(my._doc._id));
        friend.inDibInFriends.splice(i, 1);
        my.friends.push(friend._id);
        friend.friends.push(my._id);
        await my.save((err) => {
            if (err) return {status: 'error', error: err};
        });
        await friend.save((err) => {
            if (err) return {status: 'error', error: err};
        });
        return {status: 'ok'}
    } else {
        return {status: 'error'}
    }
}

async function sendDibInFriends({friendLogin = null, myLogin = null}) {
    if (friendLogin && myLogin) {
        let my = await User.findOne({login: myLogin});
        let friend = await User.findOne({login: friendLogin});
        my.outDibInFriends.push(friend._doc._id);
        friend.inDibInFriends.push(my._doc._id);
        await my.save((err) => {
            if (err) return {status: 'error', error: err};
        });
        await friend.save((err) => {
            if (err) return {status: 'error', error: err};
        });
        return {status: 'ok'}
    } else {
        return {status: 'error'}
    }

}

async function getUsers(filter = "") {
    let filt =  (filter.length>1)? new RegExp(`(${filter.replace(/\s+/g,")|(")})`, 'img'): new RegExp(`(${filter})`, 'img');
    let t = await User.find({$or: [{firstName: filt}, {secondName: filt}]});
    if (t) {
        return {status: 'ok', data: t}
    } else {
        return {status: 'error'}
    }
}

async function login({login = null, password = null}) {
    if (login && password) {
        let tmp = await User.findOne({login});
        delete tmp._doc.password;
        return {status: 'ok', data: tmp}
    } else {
        return {status: 'error'}
    }
}

async function getUserInfo({id=null}) {
    if (id) {
        let tmp = await User.findOne({_id: id});
        let fr = await getFriends(tmp.login);
        tmp.friends = [...fr.data];
        delete tmp._doc.password;
        delete tmp._doc.inDibInFriends;
        delete tmp._doc.outDibInFriends;
        return {status: 'ok', data: tmp}
    } else {
        return {status: 'error'}
    }
}

async function createUser({login = null, password = null, firstName = null, secondName = null}) {
    if (secondName && firstName && password && login) {
        let res = await User.findOne({"login": login});
        if (res) return {status: 'error', error: 'Пользователь уже существует'};
        let user = new User({
            login,
            firstName,
            secondName,
            password: hash(password),
            dateOfRegistration: new Date()
        });
        await user.save((err) => {
            if (err) return {status: 'error', error: err};
        });
        delete user.password;
        return {status: 'ok', data: user}
    } else {
        return {status: 'error'}
    }
}


const hash = (pass) => {
    return crypto.createHash('sha1')
        .update(pass).digest('base64')
};