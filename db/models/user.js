let mongoose = require('mongoose'),
    User = new mongoose.Schema({
            login: {
                type: String,
                unique: true,
                required: true
            },
            firstName: {
                type: String,
                required: true
            },
            secondName: {
                type: String,
                required: true
            },
            password: {
                type: String,
                required: true
            },
            dateOfRegistration: {
                type: Date,
                required: true
            },
            friends: {
                type: Array,
                required: false
            },
            inDibInFriends: {
                type: Array,
                required: false
            },
            outDibInFriends: {
                type: Array,
                required: false
            }
        },
        {versionKey: false});
module.exports = mongoose.model('User', User);