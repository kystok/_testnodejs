let express = require('express');
let path = require('path');
let session = require('express-session');
let logger = require('morgan');
let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');
const config = require('./config.json');

let app = express();

const MongoStore = require('connect-mongo')(session);

app.use((req,res,next)=>{
    res.setHeader('Access-Control-Allow-Origin', config.cross);
    next();
});

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({
        secret: config.secret,
        cookie: {
            httpOnly: true,
            maxAge: null
        },
        store: new MongoStore({
            host: config.db.host,
            port: config.db.port,
            db: config.db.db,
            url: config.db.url
        }),
        resave: true,
        saveUninitialized: true}
));

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(function(req, res, next) {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.use(function(err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
