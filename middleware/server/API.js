"use strict";
const config = require('../../config.json'),
    DB_API = require('../../db/API');


module.exports = {
  createUser,
    login,
    getUserInfo,
    getUsers,
    sendDibInFriends,
    infoDib,
    okAddFriend,
    getFriends,
};

async function createUser({login=null,password=null, secondName=null, firstName=null}) {

    if (login===null || password===null || secondName===null || firstName===null)
        return {status: 'error', error: 'не введены входные данные'};

    if (login.length<config.param.minLogin)
        return {status: 'error', error: 'логин короткий'};
    if (login.length>config.param.maxLogin)
        return {status: 'error', error: 'логин длинный'};

    if (password.length<config.param.minPassword)
        return {status: 'error', error: 'пароль короткий'};
    if (password.length>config.param.maxPassword)
        return {status: 'error', error: 'пароль длинный'};

    if (secondName.length<config.param.minSecondName)
        return {status: 'error', error: 'фамилия короткая'};
    if (secondName.length>config.param.maxSecondName)
        return {status: 'error', error: 'фамилия длинная'};

    if (firstName.length<config.param.minFirstName)
        return {status: 'error', error: 'Имя короткая'};
    if (firstName.length>config.param.maxFirstName)
        return {status: 'error', error: 'Имя длинная'};

    return await DB_API.createUser({login,password,secondName,firstName});

}

async function login({login=null,password=null}) {
    if (login===null || password===null )
        return {status: 'error', error: 'не введены входные данные'};

    if (login.length<config.param.minLogin)
        return {status: 'error', error: 'входные данные не верны'};
    if (login.length>config.param.maxLogin)
        return {status: 'error', error: 'входные данные не верны'};

    if (password.length<config.param.minPassword)
        return {status: 'error', error: 'входные данные не верны'};
    if (password.length>config.param.maxPassword)
        return {status: 'error', error: 'входные данные не верны'};

    return await DB_API.login({login,password})
}

async function getUserInfo({id=null}) {
    if (id===null)
        return {status: 'error', error: 'не введены входные данные'};
    return await DB_API.getUserInfo({id});
}

async function getUsers(filter="") {
    return await DB_API.getUsers(filter);
}

async function sendDibInFriends({friendLogin=null, myLogin=null}) {
    if (friendLogin===null || myLogin===null )
        return {status: 'error', error: 'не введены входные данные'};
    if (friendLogin.length<config.param.minLogin)
        return {status: 'error', error: 'логин короткий'};
    if (myLogin.length<config.param.minLogin)
        return {status: 'error', error: 'логин короткий'};
    if (myLogin.length>config.param.maxLogin)
        return {status: 'error', error: 'логин длинный'};
    if (friendLogin.length>config.param.maxLogin)
        return {status: 'error', error: 'логин длинный'};
    return DB_API.sendDibInFriends({friendLogin,myLogin})
}

async function infoDib({login=null}) {
    if (!login)
        return {status: 'error', error: 'логина нет'};
    if (login.length<config.param.minLogin)
        return {status: 'error', error: 'логин короткий'};
    if (login.length>config.param.maxLogin)
        return {status: 'error', error: 'логин длинный'};
    return DB_API.infoDib(login);
}

async function okAddFriend({friendLogin=null, myLogin=null}) {
    if (friendLogin===null || myLogin===null )
        return {status: 'error', error: 'не введены входные данные'};
    if (friendLogin.length<config.param.minLogin)
        return {status: 'error', error: 'логин короткий'};
    if (myLogin.length<config.param.minLogin)
        return {status: 'error', error: 'логин короткий'};
    if (myLogin.length>config.param.maxLogin)
        return {status: 'error', error: 'логин длинный'};
    if (friendLogin.length>config.param.maxLogin)
        return {status: 'error', error: 'логин длинный'};

    return await DB_API.okAddFriend({friendLogin, myLogin});
}

async function getFriends({login=null}) {
    if (login===null) return {status: 'error', error: 'входные данные пустые'};
    if (login.length<config.param.minLogin)
        return {status: 'error', error: 'логин короткий'};
    if (login.length>config.param.maxLogin)
        return {status: 'error', error: 'логин длинный'};
    return await DB_API.getFriends(login);

}

