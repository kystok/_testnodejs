'use strict'
const User = require('../../db/models/user.js'),
    mongoose = require('mongoose'),
    config = require('../../config.json'),
    db = mongoose.createConnection(config.db.url),
    api = require('../../db/API');
const assert = require('chai').assert;

describe('Пользователи', () => {
    const login2 = 'vasia2',
        password2 = '1234',
        firstName2 = 'Ваня',
        secondName2 = 'Охотничев',
        login1 = 'vasia',
        password1 = '1234',
        firstName1 = 'Вася',
        secondName1 = 'Обломов';
    let user1,user2,id1;

    it('Регистрация пользователя', async () => {
        await User.find({login: login1}).remove().exec();
        await User.find({login: login2}).remove().exec();
        user1 = await api.createUser({
            login: login1,
            password: password1,
            firstName: firstName1,
            secondName: secondName1
        });
        user2 = await api.createUser({
            login: login2,
            password: password2,
            firstName: firstName2,
            secondName: secondName2
        });
        id1 = user1.data._id;
        assert.equal(user1.status === 'ok', true);
    });

    it('Получение личных данных при входе в систему', async () => {
        const result = await api.login({login: login1, password: password1});
        assert.equal(result.data.login === login1, true);
    });

    it('Получение личных данных по id', async () => {
        const result = await api.getUserInfo({id:id1});
        assert.equal(result.data.login === login1, true);
    });

    it('Получение списка пользователей (всех = без фильтра)', async () => {
        let result = await api.getUsers();
        assert.equal(result.data.length > 0, true);
    });

    it('Получение списка пользователей по фильтру (имя)', async () => {
        let result = await api.getUsers("ваня");
        assert.equal(result.status === 'ok', true);
    });

    it('Получение списка пользователей по фильтру (фамилия)', async () => {
        let result = await api.getUsers("обл");
        assert.equal(result.data[0].login === login1, true);
    });

    it('Отправление заявки в друзья', async () => {
        let result = await api.sendDibInFriends({friendLogin: login1, myLogin: login2});
        assert.equal(result.status === 'ok', true);
    });

    it('Получение заявки в друзья', async () => {
        let result = await api.login({password: password1, login: login1});
        assert.equal(result.data.inDibInFriends.indexOf(user2.data._id) !==-1, true);
    });

    it('Информация о заявках', async () => {
        let result = await api.infoDib(login2);
        assert.equal(result.status==='ok', true);
    });

    it('Принять в друзя', async () => {
        let result = await api.okAddFriend({friendLogin: login1, myLogin: login2});
        assert.equal(result.status==='ok', true);
    });

    it('Получить список друзей', async () => {
        let result = await api.getFriends(login1);
        assert.equal(result.data[0].login===login2, true);
    });
});
