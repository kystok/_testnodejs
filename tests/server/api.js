const User = require('../../db/models/user.js'),
    mongoose = require('mongoose'),
    config = require('../../config.json'),
    db = mongoose.createConnection(config.db.url),
    api = require('../../middleware/server/API');
const assert = require('chai').assert;

describe('Функционал', () => {
    const login2 = 'vasia2',
        password2 = '1234',
        firstName2 = 'Ваня',
        secondName2 = 'Охотничев',
        login1 = 'vasia',
        password1 = '1234',
        firstName1 = 'Вася',
        secondName1 = 'Обломов';
    let user1,user2,id1;

    it('Регистрация пользователя', async () => {
        await User.find({login: login1}).remove().exec();
        await User.find({login: login2}).remove().exec();
        user1 = await api.createUser({
            login: login1,
            password: password1,
            firstName: firstName1,
            secondName: secondName1
        });
        user2 = await api.createUser({
            login: login2,
            password: password2,
            firstName: firstName2,
            secondName: secondName2
        });
        id1 = user1.data._id;
        assert.equal(user1.status === 'ok', true);
    });

    it('Получение личных данных при входе в систему', async () => {
        const result = await api.login({login: login1, password: password1});
        assert.equal(result.data.login === login1, true);
    });

    it('Получение личных данных по id', async () => {
        const result = await api.getUserInfo({id:id1});
        assert.equal(result.data.login === login1, true);
    });

    it('Получение списка пользователей (всех = без фильтра)', async () => {
        let result = await api.getUsers();
        assert.equal(result.data.length > 0, true);
    });

    it('Получение списка пользователей по фильтру (имя)', async () => {
        let result = await api.getUsers("ваня");
        assert.equal(result.status === 'ok', true);
    });

    it('Получение списка пользователей по фильтру (фамилия)', async () => {
        let result = await api.getUsers("обл");
        assert.equal(result.status === 'ok', true);
    });

    it('Отправление заявки в друзья', async () => {
        let result = await api.sendDibInFriends({friendLogin: login1, myLogin: login2});
        assert.equal(result.status === 'ok', true);
    });

    it('Информация о заявках', async () => {
        let result = await api.infoDib({login: login2});
        assert.equal(result.status==='ok', true);
    });

    it('Принять в друзя', async () => {
        let result = await api.okAddFriend({friendLogin: login1, myLogin: login2});
        assert.equal(result.status==='ok', true);
    });

    it('Получить список друзей', async () => {
        let result = await api.getFriends({login: login1});
        assert.equal(result.data[0].login===login2, true);
    });
});

describe('Обработка ошибок', () => {
    const abc='qwertyuiopasdfghjklzxcvbnm,wertyuio',
        login1_MIN = abc.substring(0,config.param.minLogin-1),
        password1_MIN = abc.substring(1,config.param.minPassword-1),
        firstName1_MIN = abc.substring(2,config.param.minFirstName-1),
        secondName1_MIN = abc.substring(3,config.param.minSecondName-1),
        login1_MAX = abc.substring(0,config.param.maxLogin+1),
        password1_MAX =abc.substring(1,config.param.maxPassword+1),
        firstName1_MAX = abc.substring(2,config.param.maxFirstName+1),
        secondName1_MAX = abc.substring(3,config.param.maxSecondName+1),
        login1 = abc.substring(3,config.param.minLogin+1),
        password1 = abc.substring(4,config.param.minPassword+1),
        firstName1 = abc.substring(5,config.param.minFirstName+1),
        secondName1 = abc.substring(6,config.param.minSecondName+1);
    let user1,id1;
    describe('Регистрация', () => {
        it('Ошибка при регистрация без логина', async () => {
            user1 = await api.createUser({
                password: password1,
                firstName: firstName1,
                secondName: secondName1
            });
            assert.equal(user1.status === 'error', true);
        });

        it('Ошибка при регистрация без пароля', async () => {
            user1 = await api.createUser({
                login: login1,
                firstName: firstName1,
                secondName: secondName1
            });
            assert.equal(user1.status === 'error', true);
        });

        it('Ошибка при регистрация без имени', async () => {
            user1 = await api.createUser({
                login: login1,
                password: password1,
                secondName: secondName1
            });
            assert.equal(user1.status === 'error', true);
        });

        it('Ошибка при регистрация без фамилии', async () => {
            user1 = await api.createUser({
                login: login1,
                password: password1,
                firstName: firstName1,
            });
            assert.equal(user1.status === 'error', true);
        });

        it('Ошибка при регистрация с коротким логином', async () => {
            user1 = await api.createUser({
                login: login1_MIN,
                password: password1,
                firstName: firstName1,
                secondName: secondName1
            });
            assert.equal(user1.status === 'error', true);
        });

        it('Ошибка при регистрация с коротким паролем', async () => {
            user1 = await api.createUser({
                login: login1,
                password: password1_MIN,
                firstName: firstName1,
                secondName: secondName1
            });
            assert.equal(user1.status === 'error', true);
        });

        it('Ошибка при регистрация с коротким именем', async () => {
            user1 = await api.createUser({
                login: login1,
                password: password1,
                firstName: firstName1_MIN,
                secondName: secondName1
            });
            assert.equal(user1.status === 'error', true);
        });

        it('Ошибка при регистрация с короткой фамилией', async () => {
            user1 = await api.createUser({
                login: login1,
                password: password1,
                firstName: firstName1,
                secondName: secondName1_MIN
            });
            assert.equal(user1.status === 'error', true);
        });

        it('Ошибка при регистрация с длинным логином', async () => {
            user1 = await api.createUser({
                login: login1_MAX,
                password: password1,
                firstName: firstName1,
                secondName: secondName1
            });
            assert.equal(user1.status === 'error', true);
        });

        it('Ошибка при регистрация с длинным паролем', async () => {
            user1 = await api.createUser({
                login: login1,
                password: password1_MAX,
                firstName: firstName1,
                secondName: secondName1
            });
            assert.equal(user1.status === 'error', true);
        });

        it('Ошибка при регистрация с длинным именем', async () => {
            user1 = await api.createUser({
                login: login1,
                password: password1,
                firstName: firstName1_MAX,
                secondName: secondName1
            });
            assert.equal(user1.status === 'error', true);
        });

        it('Ошибка при регистрация с длинным фамилией', async () => {
            user1 = await api.createUser({
                login: login1,
                password: password1,
                firstName: firstName1,
                secondName: secondName1_MAX
            });
            assert.equal(user1.status === 'error', true);
        });
    });

    describe('Вход', () => {
        it('Ошибка при входе без логина', async () => {
            user1 = await api.login({
                password: password1
            });
            assert.equal(user1.status === 'error', true);
        });
        it('Ошибка при входе без пароля', async () => {
            user1 = await api.login({
                login: login1
            });
            assert.equal(user1.status === 'error', true);
        });

        it('Ошибка при входе с коротким паролем', async () => {
            user1 = await api.login({
                login: login1,
                password: password1_MIN,
            });
            assert.equal(user1.status === 'error', true);
        });
        it('Ошибка при входе с коротким логином', async () => {
            user1 = await api.login({
                login: login1_MIN,
                password: password1,
            });
            assert.equal(user1.status === 'error', true);
        });

        it('Ошибка при входе с длинным паролем', async () => {
            user1 = await api.login({
                login: login1,
                password: password1_MAX,
            });
            assert.equal(user1.status === 'error', true);
        });
        it('Ошибка при входе с длинным логином', async () => {
            user1 = await api.login({
                login: login1_MAX,
                password: password1,
            });
            assert.equal(user1.status === 'error', true);
        });
    });

    describe('Получение списка друзей', () => {
        it('Ошибка без логина', async () => {
            user1 = await api.getFriends({});
            assert.equal(user1.status === 'error', true);
        });
        it('Ошибка с коротким логином', async () => {
            user1 = await api.getFriends({
                login: login1_MIN,
            });
            assert.equal(user1.status === 'error', true);
        });
        it('Ошибка с длинным логином', async () => {
            user1 = await api.getFriends({
                login: login1_MAX,
            });
            assert.equal(user1.status === 'error', true);
        });
    });
    describe('Подтверждение заявки в друзья', () => {
        it('Ошибка без логина своего', async () => {
            user1 = await api.okAddFriend({
                friendLogin: login1
            });
            assert.equal(user1.status === 'error', true);
        });
        it('Ошибка без логина друга', async () => {
            user1 = await api.okAddFriend({
                myLogin: login1
            });
            assert.equal(user1.status === 'error', true);
        });
        it('Ошибка с коротким своим логином', async () => {
            user1 = await api.okAddFriend({
                myLogin: login1_MIN,
                friendLogin: login1,
            });
            assert.equal(user1.status === 'error', true);
        });
        it('Ошибка с коротким друга логином', async () => {
            user1 = await api.okAddFriend({
                myLogin: login1,
                friendLogin: login1_MIN,
            });
            assert.equal(user1.status === 'error', true);
        });
        it('Ошибка с длинным своим логином', async () => {
            user1 = await api.okAddFriend({
                myLogin: login1_MAX,
                friendLogin: login1,
            });
            assert.equal(user1.status === 'error', true);
        });
        it('Ошибка с длинным друга логином', async () => {
            user1 = await api.okAddFriend({
                myLogin: login1,
                friendLogin: login1_MAX,
            });
            assert.equal(user1.status === 'error', true);
        });
    });

    describe('Получение объекта с исходящими и входящими заявками', () => {
        it('Ошибка без логина', async () => {
            user1 = await api.infoDib({});
            assert.equal(user1.status === 'error', true);
        });
        it('Ошибка длинный логин', async () => {
            user1 = await api.infoDib({
                login: login1_MAX
            });
            assert.equal(user1.status === 'error', true);
        });
        it('Ошибка короткий логин', async () => {
            user1 = await api.infoDib({
                login: login1_MIN
            });
            assert.equal(user1.status === 'error', true);
        });

    });

    describe('Отправление заявки в друзья', () => {
        it('Ошибка без логина своего', async () => {
            user1 = await api.sendDibInFriends({
                friendLogin: login1
            });
            assert.equal(user1.status === 'error', true);
        });
        it('Ошибка без логина друга', async () => {
            user1 = await api.sendDibInFriends({
                myLogin: login1
            });
            assert.equal(user1.status === 'error', true);
        });
        it('Ошибка с коротким своим логином', async () => {
            user1 = await api.sendDibInFriends({
                myLogin: login1_MIN,
                friendLogin: login1,
            });
            assert.equal(user1.status === 'error', true);
        });
        it('Ошибка с коротким друга логином', async () => {
            user1 = await api.sendDibInFriends({
                myLogin: login1,
                friendLogin: login1_MIN,
            });
            assert.equal(user1.status === 'error', true);
        });
        it('Ошибка с длинным своим логином', async () => {
            user1 = await api.sendDibInFriends({
                myLogin: login1_MAX,
                friendLogin: login1,
            });
            assert.equal(user1.status === 'error', true);
        });
        it('Ошибка с длинным друга логином', async () => {
            user1 = await api.sendDibInFriends({
                myLogin: login1,
                friendLogin: login1_MAX,
            });
            assert.equal(user1.status === 'error', true);
        });
    });


    /*it('Получение личных данных при входе в систему', async () => {
        const result = await api.login({login: login1, password: password1});
        assert.equal(result.data.login === login1, true);
    });

    it('Получение личных данных по id', async () => {
        const result = await api.getUserInfo({id:id1});
        assert.equal(result.data.login === login1, true);
    });

    it('Получение списка пользователей (всех = без фильтра)', async () => {
        let result = await api.getUsers();
        assert.equal(result.data.length > 0, true);
    });

    it('Получение списка пользователей по фильтру (имя)', async () => {
        let result = await api.getUsers("ваня");
        assert.equal(result.data[0].login === login2, true);
    });

    it('Получение списка пользователей по фильтру (фамилия)', async () => {
        let result = await api.getUsers("обл");
        assert.equal(result.data[0].login === login1, true);
    });

    it('Отправление заявки в друзья', async () => {
        let result = await api.sendDibInFriends({friendLogin: login1, myLogin: login2});
        assert.equal(result.status === 'ok', true);
    });

    it('Информация о заявках', async () => {
        let result = await api.infoDib(login2);
        assert.equal(result.status==='ok', true);
    });

    it('Принять в друзя', async () => {
        let result = await api.okAddFriend({friendLogin: login1, myLogin: login2});
        assert.equal(result.status==='ok', true);
    });

    it('Получить список друзей', async () => {
        let result = await api.getFriends(login1);
        assert.equal(result.data[0].login===login2, true);
    });*/
});
